# UBports Development Scripts

## Release Management

The UBports release management scripts are used to nearly fully automize
Lomiri component releases and Ubuntu Touch App releases. This includes app
releases to Open-Store.io as well as releases of DEB-packaged core
components of Ubuntu Touch / Lomiri.

The idea of a release in Git is creating a dedicated release commit which
updates metadata in the source tree, such as:

  * ChangeLog file
  * NEWS file
  * AUTHORS file
  * debian/changelog
  * component version string(s)
  * date in man pages (FIXME! Not yet implemented)
  * etc.pp.

This commit can be created 90% automatically, but it needs at least 10%
of thorough review by a pair of human eyes. The release script
``ubports_mkrelease`` guides you through this process.

### Pre-requisites for using the UBports release scripts

The UBports release scripts have only been used and tested on GNU/Linux
so far. If you dare using them on other operating systems, please know
that we won't support that. We happily accept patches, though.

#### Prepare / install the release scripts

Use ``git clone <LINK>`` to create a local working copy of this UBports
Core DevScripts repository.

For being able to use the release scripts shipped in this Git repo, run
``install-symlinks-to-home.sh``. That little installer script creates
symlinks in ``~/bin/`` that point to your local Git working copy of this repository.

The script can be adjusted to your needs, in case your executables rest
e.g. in ``~/.local/bin``. Feel free to tweak it before executing it.

#### GnuPG is required

Make sure you have a working GnuPG setup. You need to be able to sign Git
tags on your system / with your user account.

Note, that at the moment, the scripts don't check whether you have
a properly working GnuPG setup. Simply make sure you have one!

For further info, see https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work

### Cutting a Lomiri component or Ubuntu Touch App release

  1. Make sure you have an up-to-date local Git working copy of the
     Lomiri or Ubuntu Touch component that you want to release.
  2. Enter the components/apps folder which is going to be released
     and make sure no build-cruft or other not-committed files are
     in the file tree.
  3. When ready + sure, run ``ubports_mkrelease <next_version>``
  4. Let yourself be walked through the steps shown on screen.

The overall workflow is that you run ``ubports_mkrelease
<next_version>``. You end up in an intermediate shell environment where
you can apply more changes to the release commit (``git show; editor
<some-file>; git commit --amend --patch``, etc.).

When done with manual adjustments and fully sure you are good to go, type:

```
user@host:~ > exit 10
```

This now will finalize the release commit and eventually create a GnuPG-signed release tag in Git.

If you don't want to cut the release, you can bail out of the intermediate shell with 

```
user@host:~ > exit 0
```

(instead of ``exit 10``). This will undo all the changes previously
applied by the ``ubports_mkrelease`` script.

#### Hey, this didn't work...

If something goes wrong during the above workflow, please report the
observed issue here. All Lomiri / Ubuntu Touch components differ in
various parts, so it may happen that these release scripts have not yet
been tested against the component you tried to release. The overall idea
is to have a very general tool that can handle all sorts of Lomiri /
Ubuntu Touch components, so component releases become easy and uniform.

## More Helper Scripts

### White-space clean-up in commits

Befor committing code, you can use ``ubports_fix-white-space`` to
whitespace-clean up your code changes before committing them. Thanks to
Keith Packard from the X.org team for this.

### Find+replace

With the ``ubports_find+replace <search-phrase> <replace-by>`` you can
systematically walk over code in the current folder and do fast search +
replace operations. Handle with care, use ``git reset --hard`` to undo
your changes.

This script is the nice (but not perfect) helper for forking code, i.e.
renaming a project to a new name.
