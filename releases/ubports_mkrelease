#!/bin/bash

set -e

if [ -n "$UBPORTS_DEBUG" ]; then
	set -x
fi

# Copyright (C) 2021-2023 UBports Foundation
# Author: Mike Gabriel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

function _get_release_notes_from_news_file() {
set -x
	if [ -e NEWS ]; then
		# Obtain the first changes block from the NEWS file...
		RELEASE_NOTES=`awk '{ if ($0 == "") ++b; if($1 != "Overview" && $0 != "" && b <= 2) { print $0; }}' NEWS`
	fi
	if [ -n "${RELEASE_NOTES}" ]; then
		RELEASE_COMMIT_MESSAGE="$(echo -e "Release ${NEW_RELEASE_VERSION}\n\n${RELEASE_NOTES}")"
	else
		RELEASE_COMMIT_MESSAGE="Release ${NEW_RELEASE_VERSION}"
	fi
	RELEASE_TAG_MESSAGE="${RELEASE_COMMIT_MESSAGE}"
set +x
}

# colors
Color_Off='\e[0m'       # Text Reset

Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue

PROG_NAME="$(basename "$0")"
NEW_RELEASE_VERSION="$1"

# Strip leading v from version string (e.g. v1.2.3 -> 1.2.3)
NEW_RELEASE_VERSION=`echo "${NEW_RELEASE_VERSION}" | sed -re "s/^v(.*)/\1/"`

if [ -z "$NEW_RELEASE_VERSION" ]; then
	echo "usage: $(basename $0) <release-version>"
	exit -1
fi

# Detect whether to use v1.2.3 or just 1.2.3 as release version format.
previous_tag=$(git tag | sort -V | tail -n1)
if echo ${previous_tag} | grep -q -E '^v[0-9\.]+$'; then
	v_prefix="v"
else
	v_prefix=""
fi

NEW_RELEASE_TAG="${v_prefix}${NEW_RELEASE_VERSION}"

if ! which git 1>/dev/null; then
	echo -e "${Red}ERROR: the Git command line tool is not installed. You are being serious, aren't you???${Color_Off}"
	exit -1
fi

if ! which dpkg-parsechangelog 1>/dev/null; then
	echo -e "${Red}ERROR: the Debian command line tool 'dpkg-parsechangelog' is not installed. Can't go on...${Color_Off}"
	exit -1
fi

if ! which dch 1>/dev/null; then
	echo -e "${Red}ERROR: the Debian command line tool 'debchange' (aka 'dch') is not installed. Can't go on...${Color_Off}"
	exit -1
fi

# FIXME: Check that we have a working GnuPG setup in the user's home directory.

orig_d="$(pwd)"

d=. ; while [ ! -d ${d}/.git -a "$(readlink -e ${d})" != "/" ]; do d=${d}/..; done

if [ -d "$d/.git" ]; then

	cd "${d}"

	LAST_VERSION="$(ubports_detectcurrentversion)"

	if [ -d "debian/" ]; then
		LAST_DISTRIBUTION="$(dpkg-parsechangelog -SDistribution)"

		LAST_PKG_UPSTREAM_VERSION="$(dpkg-parsechangelog -SVersion | sed -Ee 's/([0-9]*:|)([0-9\.]+)(\-.*|)/\2/')"

		if dpkg --compare-versions ${LAST_PKG_UPSTREAM_VERSION} lt ${LAST_VERSION} || \
		   { [ -z "${LAST_VERSION}" ] && [ -n "${LAST_PKG_UPSTREAM_VERSION}" ]; }; then
			LAST_VERSION="${LAST_PKG_UPSTREAM_VERSION}"
		fi
	else
		LAST_DISTRIBUTION="NOT-FOUND"
	fi
	CURRENT_BRANCH="$(git branch --show-current)"

	echo
	# FIXME: is there a reliable way to detect a software component's name (except from
	#        looking into debian/changelog?
	if [ -e debian/changelog ]; then
		echo -e "Software project: ${Yellow}$(dpkg-parsechangelog -S Source 2>/dev/null)${Color_Off}"
	fi
	echo -e "Current Git branch: ${Yellow}${CURRENT_BRANCH}${Color_Off}"
	echo
	if [ "${LAST_DISTRIBUTION}" = "UNRELEASED" ] && [ "${LAST_VERSION}" = "${NEW_RELEASE_VERSION}" ]; then
		echo -e "LAST_VERSION is: ${Yellow}${LAST_VERSION}${Color_Off} (already pre-released)"
	else
		echo -e "LAST_VERSION is: ${Yellow}${LAST_VERSION}${Color_Off}"
	fi
	echo -e "LAST_PKG_UPSTREAM_VERSION is: ${Yellow}${LAST_PKG_UPSTREAM_VERSION}${Color_Off}"
	echo -e "NEW_RELEASE_VERSION is: ${Yellow}${NEW_RELEASE_VERSION}${Color_Off}"
	echo

	if git tag | grep -q -E -e "^${NEW_RELEASE_VERSION}\$" -e "^${NEW_RELEASE_TAG}\$"; then
		echo -e "${Red}ERROR: ${Color_Off}A version ${NEW_RELEASE_VERSION} has already been released."
		echo -e "${Red}ERROR: ${Color_Off}Check output of 'git tag'."
		exit 1
	fi

	if [ "${LAST_VERSION}" = "${NEW_RELEASE_VERSION}" ] && [ "${LAST_DISTRIBUTION}" != "UNRELEASED" ] && [ "${LAST_DISTRIBUTION}" != "NOT-FOUND" ]; then
		echo -e "${Red}ERROR: ${Yellow}The new release version is the same as the current version. Something went wrong here...${Color_Off}"
		echo
		exit 1
	fi

	modified_files_count=$(git status -s | wc -l)
	if [ ${modified_files_count} -gt 0 ]; then
		echo
		echo -e "${Red}ERROR: ${Yellow}Some local changes are not commited:${Color_Off}"
		echo
		git status
		echo
		echo -e "${Red}ERROR: ${Yellow}Can't do the release with this. Please fix!${Color_Off}"
		echo
		exit 1
	fi

	if [ "${LAST_VERSION}" != "${NEW_RELEASE_VERSION}" ]; then
		ubports_find+replace "${LAST_VERSION}" "${NEW_RELEASE_VERSION}"
		echo -e "${Green}success: ${Yellow}New upstream version number set in the code files (where appropriate).${Color_Off}"
	fi

	# creation / initial update of upstream ChangeLog
	if [ ! -e ChangeLog ]; then
		touch ChangeLog
		git add ChangeLog
		if ubports_mkchangelog; then
			echo -e "${Green}success: ${Yellow}Upstream ChangeLog created.${Color_Off}"
		else
			echo -e "${Red}ERROR: ${Color_Off}Failure during upstream ChangeLog creation."
			cd "${orig_d}"
			exit 1
		fi
	else
		if ubports_mkchangelog; then
			echo -e "${Green}success:${Yellow} Upstream ChangeLog created.${Color_Off}"
		else
			echo -e "${Red}ERROR: ${Color_Off}Failure during upstream ChangeLog creation."
			cd "${orig_d}"
			exit 1
		fi
	fi

	# creation / update of upstream AUTHORS file
	if [ ! -e AUTHORS ]; then
		touch AUTHORS
		git add AUTHORS
		if ubports_mkauthors; then
			echo -e "${Green}success: ${Yellow}Upstream AUTHORS file created.${Color_Off}"
		else
			echo -e "${Red}ERROR: ${Color_Off}Failure during AUTHORS file creation."
			cd "${orig_d}"
			exit 1
		fi
	else
		if ubports_mkauthors; then
			echo -e "${Green}success: ${Yellow}Upstream AUTHORS file updated for new release.${Color_Off}"
		else
			echo -e "${Red}ERROR: ${Color_Off}Failure during AUTHORS file creation."
			cd "${orig_d}"
			exit 1
		fi
	fi

	# If present, enforce release manager to write some meaningful
	# information into the upstream NEWS file (basically a shortened
	# extract of the recent changes found in updated ChangeLog.
	if [ -e NEWS ]; then
		ubports_mknewsdraft "${NEW_RELEASE_VERSION}"
		editor NEWS
		echo -e "${Green}success: ${Yellow}Upstream NEWS file updated for new release.${Color_Off}"
	else
		echo -e "${Yellow}warning:${Color_Off} No NEWS file found, please consider adding one."
	fi

	if [ -e "debian/changelog" ]; then
		if ubports_mkdebchangelog ${NEW_RELEASE_VERSION}; then
			echo -e "${Green}success: ${Yellow}debian/changelog file has been updated.${Color_Off}"
		else
			echo -e "${Red}ERROR:${Color_Off} Failure while updating debian/changelog."
			cd "${orig_d}"
			exit 1
		fi
	fi

	# Retrieve the redacted changes block from the NEWS file.
	_get_release_notes_from_news_file

	# Do the initial release commit and tag it with a fake tag
	git commit -a -m "${RELEASE_COMMIT_MESSAGE}"
	echo -e "${Green}success: ${Yellow}Release commit checked in.${Color_Off}"

	git tag "${NEW_RELEASE_TAG}" -m "${RELEASE_TAG_MESSAGE}"
	echo -e "${Green}success: ${Yellow}Fake release tag set.${Color_Off}"

	# Update the ChangeLog, now with the correct release tag
	if ubports_mkchangelog; then
		echo -e "${Green}success:${Yellow} Upstream ChangeLog updated/finalized.${Color_Off}"
	else
		echo -e "${Red}ERROR: ${Color_Off}Failure during upstream ChangeLog update."
		cd "${orig_d}"
		exit 1
	fi

	echo -e "${Green}success: ${Yellow}Upstream ChangeLog refreshed (now with release commit and release tag).${Color_Off}"
	# Amend the release commit with correct upstream ChangeLog
	git commit -a --amend -m "${RELEASE_COMMIT_MESSAGE}"
	echo -e "${Green}success: ${Yellow}Release commit updated with fixed upstream ChangeLog.${Color_Off}"
	# Roll back the fake release tag
	git tag -d "${NEW_RELEASE_TAG}"
	echo -e "${Green}success: ${Yellow}Fake release tag removed again.${Color_Off}"

	echo
	echo -e "${Yellow}NOTE: ${Blue}Dropping into a sub-shell now for manual introspection...${Color_Off} This is normal routine and your safety net!"
	echo
	echo -e "${Yellow}Now, please check the applied changes using '${Color_Off}git show${Yellow}' and adjust things as needed.${Color_Off}"
	echo -e "${Yellow}Type '${Color_Off}exit 10${Yellow}' for doing the release. Type '${Color_Off}exit${Yellow}' to abort the release process.${Color_Off}"
	echo
	echo -e "${Yellow}NOTE: ${Color_Off}The release commit will be amended with your manual adjustments, if any."
	echo

	# Show a custom prompt to signify that they're in a subshell. To avoid
	# .bashrc overwriting our custom prompt, pass --norc to bash.
	# https://unix.stackexchange.com/a/6807
	PS1="(${PROG_NAME}) ${PS1}" bash --norc -i || ret=$?

	if [ "$ret" = "10" ]; then
		echo
		echo -e "${Green}USER ACK: ${Yellow}Doing the release now...${Color_Off}"
		echo
	else
		echo
		echo -e "${Red}USER N'ACK: ${Yellow}Rolling back all auto-generated changes.${Color_Off}"
		echo
		git reset --hard HEAD~ 1>/dev/null
		git clean -df 1>/dev/null
		echo
		echo -e "${Yellow}ROLLBACK COMPLETED: ${Green}You are back from where you started.${Color_Off}"
		echo
		cd "${orig_d}"
		exit 1
	fi

	# Re-retrieve the (maybe interactively modified) redacted changes block from the NEWS file.
	_get_release_notes_from_news_file

	# Amend the release commit again with manually applied adjustments, also if no
	# changes have been made. This is to definitely update the commit message from the NEWS
	# file which might have been added or modified during the interactive bash above.
	git commit -a --amend -m "${RELEASE_COMMIT_MESSAGE}"
	echo -e "${Green}success: ${Yellow}Release commit amended, finally.${Color_Off}"

	echo -e "${Yellow}NOTE: ${Color_Off}Watch out for GnuPG's password dialog window (**askpass)!${Color_Off}"
	echo

	# Finally, add the real release tag
	git tag -s "${NEW_RELEASE_TAG}" -m "${RELEASE_TAG_MESSAGE}"
	echo -e "${Green}success: ${Yellow}Official release tag '${Color_Off}${NEW_RELEASE_TAG}${Yellow}' set now.${Color_Off}"

	echo
	echo -e "${Yellow}IMPORTANT: ${Green}The release process on your local Git working copy is now complete."
	echo -e "           ${Yellow}Don't forget to push your local changes: ${Color_Off}git push <remote> ${CURRENT_BRANCH}:${CURRENT_BRANCH} && git push <remote> --tags${Yellow}.${Color_Off}"
	echo

	echo -e "${Yellow}UNDO HOWTO: Undo local release changes with these two commands: '${Color_Off}git reset --hard HEAD~; git tag -d ${NEW_RELEASE_TAG}${Yellow}'.${Color_Off}"
	echo

	cd "${orig_d}"

fi

exit 0
